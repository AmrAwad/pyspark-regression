from pyspark import SparkConf, SparkContext
from pyspark.sql import SQLContext
from pyspark.ml.feature import VectorAssembler
from pyspark.ml.regression import LinearRegression
from pyspark.sql.functions import monotonically_increasing_id


# Read the CSV file into a dataframe
sc= SparkContext()
sqlContext = SQLContext(sc)
sample_df = sqlContext.read.format('com.databricks.spark.csv').options(header='true', inferschema='true').load('Sample.csv')

output_df = None
# For each unique combination of BuildingId & ThermostatId, build a ML model and do predicitons
for thermostat in sample_df.select(['BuildingId', 'ThermostatId']).drop_duplicates().collect():

    # Get the thermostat data
    thermostat_df = sample_df.where((sample_df.BuildingId == thermostat.BuildingId) & (sample_df.ThermostatId == thermostat.ThermostatId))

    # Break the data into features (Sensor 1 & Sensor 2) & the output is the temperature
    vectorAssembler = VectorAssembler(inputCols = ['Sensor1', 'Sensor2'], outputCol = 'features')
    vsample_df = vectorAssembler.transform(thermostat_df)
    vsample_df = vsample_df.select(['features', 'Temperature'])

    # Split the data into training & testing
    splits = vsample_df.randomSplit([0.6, 0.4])
    train_df = splits[0]
    test_df = splits[1]

    # Build a linear regression model and fit it to the data.
    lr = LinearRegression(featuresCol='features', labelCol='Temperature', maxIter=10, regParam=0.3, elasticNetParam=0.8)
    lr_model = lr.fit(train_df)

    # Predict the temperature for all the data in the original dataframe
    lr_predictions = lr_model.transform(vsample_df)

    # Add the predections to the original dataframe by creating a common index then using join
    thermostat_df = thermostat_df.withColumn("row_idx", monotonically_increasing_id())
    lr_predictions = lr_predictions.withColumn("row_idx", monotonically_increasing_id())

    final_df = thermostat_df.join(lr_predictions.select(["prediction", "row_idx"]), "row_idx").drop("row_idx")
    
    if output_df:
        output_df = output_df.union(final_df)
    else:
        output_df = final_df

output_df.show()
