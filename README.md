# Basic Linear regression with Pyspark

This project will show how to do linear regression to predict the temperature based on multiple sensor input data.

THe project is in Python 2.7

## Setup 

1. Set up a virtual environment 
   ```
   C:\Python27\python.exe -m virtualenv venv
   ```
2. Install the required packages from `requirements.txt`:
   ```
   python -m pip install -r requirements.txt
   ```

## Running the script

1. Activate the virtual environment
    ```
    venv\scripts\activate
    ```
2. Replace `Sample.csv` with the input data
3. Run the script
   ```
   (venv) python main.py
   ```